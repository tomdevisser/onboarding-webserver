#!/bin/bash

code --install-extension adamwalzer.scss-lint
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension bradlc.vscode-tailwindcss
code --install-extension chouzz.vscode-better-align
code --install-extension dbaeumer.vscode-eslint
code --install-extension EditorConfig.EditorConfig
code --install-extension esbenp.prettier-vscode
code --install-extension GitHub.copilot
code --install-extension ms-vsliveshare.vsliveshare
code --install-extension Natizyskunk.sftp
code --install-extension neilbrayfield.php-docblocker
code --install-extension ritwickdey.LiveServer
code --install-extension shevaua.phpcs
code --install-extension waderyan.gitblame