#!/bin/bash

# For developers, run:
# ssh <user>@<host> 'bash -s' < ./strl-onboarding.sh -- -d
is_dev=false

# Check if we should run the developer script
while getopts ":d" option; do
  case $option in
    d)
      is_dev=true
      ;;
  esac
done

# Install Xcode Command Line Tools
xcode-select --install

if [ "$is_dev" = true ]; then
  # Install NVM, Node and NPM
  curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash \
    && echo 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
  [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm' >> ~/.zshrc \
    && source ~/.zshrc \
    && nvm install node

  # Create the desired folder structure
  mkdir ~/Developer/
  mkdir -p ~/Sites/webserver/sites

  # Download the package.json and package-lock.json and install NPM packages
  git clone https://gitlab.com/tomdevisser/onboarding-webserver.git ~/Sites/webserver/tmp \
    && mv ~/Sites/webserver/tmp/* ~/Sites/webserver/ \
    && rm -r ~/Sites/webserver/tmp/ \
    && cd ~/Sites/webserver \
    && npm i

  # Install global NPM packages
  npm -i -g gulp-cli
fi

# Install Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Install Homebrew casks
brew install --cask perimeter81 slack 1password google-chrome

# Install casks only for developers and support
if [ "$is_dev" = true ]; then
  brew install --cask visual-studio-code sourcetree
fi
